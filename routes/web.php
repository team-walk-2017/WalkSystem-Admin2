<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['revalidate']], function () {
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
  
Route::get('/home',function(){
	$data = DB::table('epppayment')
				->select( DB::raw('MONTHNAME(datestamp) as label'), DB::raw('SUM(amount) as y'))
				->groupBy(DB::raw('MONTH(datestamp)'))
				->get();

	return view('home')->with('data',$data);
});

/*Route::get('/home', 'HomeController@index')->name('home');*/
Route::get('/table-for-stories', 'HomeController@review')->name('review');
Route::get('/table-for-pending', 'HomeController@pendingStories')->name('pendingStories');

/*for modal*/
// for modal that can input exact needed amount
Route::get('/view-for-review-candidates/{id}', 'modalController@Viewcandidates');
// for modal that can view pending candidates
Route::get('/validate/pending/candidates/{id}', 'modalController@ValidatePendingCandidates');
Route::get('/uploads/reject/post/', 'modalController@Viewcandidates');/*ajax*/
Route::post('/decline-candidates', 'modalController@deletestories');
Route::post('/approve-candidates', 'modalController@approvestories')->name('approve-candidates');


/*admin*/
// for modal that can input exact needed amount
Route::get('/view-pending-candidates/{id}', 'modalController@ViewPendingCandidates');
// for modal that can view approved candidates
Route::get('/view-approved-candidates/{id}', 'modalController@ViewApprovedCandidates');
Route::post('/approve-pending-candidates', 'modalController@approvePendingStories')->name('approve-pending-candidates');
Route::get('/table-for-approved-request', 'HomeController@AcceptPending')->name('AcceptPending');
Route::get('/table-for-approvedrequest', 'HomeController@ApprovedRequest')->name('approvedrequest');
Route::get('/audit-trail',function(){
            $logs = \App\AuditTrail::orderBy('ID', 'DESC')->get();
            return view('auth.auditLogs')->with('logs',$logs);
        });
/*Uploading*/
Route::get('/upload-photos-videos', 'HomeController@UploadSection')->name('UploadSection');
Route::post('/upload', 'HomeController@Upload')->name('Upload');


});