@extends('layouts.main')
@section('content')
<link href="{{URL::asset('css/custom-table.css')}}" rel="stylesheet">
<div class="col-lg-12">
    @include('validator.review.approvedRequest')
</div>



<!-- modal for the candidates -->
 <div id="PendingCandidates" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="ViewPendingCandidates">
                <form method="POST" action="">
                {{ csrf_field() }}
                @include('validator.review.modal.view_candidates')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- view modal para sa mga candidates -->
<script type="text/javascript">

	 function AcceptPending(id){
        var url = "/view-approved-candidates/" + id;
            $.ajax({
               url: url,
               type: 'get',            
               beforeSend: function() {
                $('#ViewPendingCandidates').html('<center><img src="{!! URL::asset("/assets/images/loading.gif") !!}" height="100" width="100"></center>');
               },
               success: function(response) {
                $('#ViewPendingCandidates').html(response);
               }
            });
            return false;
        }

function approvePendingStories(){
   var id = $('.img-id').val();
   var token = $("[name='_token']").eq(0).val();
   var url = '/approve-pending-candidates';
   $.ajax({
       url: url,
       type: 'post',
       data: {
           id: id,
           _token: token
       },
       beforeSend: function(){
           $('#ViewPendingCandidates').html('<center><img src="{!! URL::asset("/assets/images/loading.gif") !!}" height="100" width="100"></center>');
       },
       success: function(response){
           // console.log(response);
           if(response){
               location.reload();
           }
       }
   });
}


</script>












@stop