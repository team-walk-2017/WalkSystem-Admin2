<div class="card">
        <div class="card-body">
            <h2 class="card-title">Review Stories</h2>
            <div class="table-responsive m-t-40">
                <table id="tbl_forReview" class="table table-bordered table-striped " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>Amount Needed</th>
                            <th>age</th>
                            <th>gender</th>
                            <th>address</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($forreview as $forreviews)
                        <tr>
                            <td>{{ $forreviews->id }}</td>
                            <td>{{ $forreviews->name }}</td>
                            <td>{{ $forreviews->amount }}</td>
                            <td>{{ $forreviews->age }}</td>
                            <td>{{ $forreviews->gender }}</td>
                            <td>{{ $forreviews->address }}</td>
                       
                            <td>
                                <button class="btn btn-default btn-icon add-tooltip" data-target="#candidates" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="View Gateway ID" onclick="seecandidates({{{$forreviews->id}}})">
                            <i class="ti-eye"></i>
                            </button>
              
                            
                      </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

