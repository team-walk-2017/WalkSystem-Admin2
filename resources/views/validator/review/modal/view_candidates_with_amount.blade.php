@if(!empty($candidates))

{{ csrf_field() }}

<div class="form-group row">
    <div class="modal-header">
        <h4>{{ isset($candidates->name) == true ? $candidates->name : '' }}</h4>
    </div>   
</div>



 <div class="text-center">
<?php
   
    // $File =  base64_decode(file_get_contents(env('WALK_WEBSITE')."/pictures/".$candidates->imagename));
    $File = env('WALK_WEBSITE')."/pictures/Beneficiaries/".$candidates->imagename;
   /* dd($File);*/
?>

    <center><img src="http://localhost:80/WalkSystem/public{{ '/pictures/Beneficiaries/'.$candidates->imagename }}" alt="" class="img-responsive" style="height:60%; width:70%;" name="image" /></center>
</div>

<div style="margin-bottom: 30px;"></div>
<div align="center" style="font-size: 30px">My Story</div>

<div class="form-group">
    <div class="form-group">
        <input type="hidden" class="img-id" name="id" id="id" value="{{ isset($candidates->id) == true ? $candidates->id : '' }}">
        <!-- <input type="hidden" name="_token" id="token" value="{{ csrf_field() }}"> -->
        
        <p style="font-size: 80%; text-align: justify;">{{ isset($candidates->story) == true ? $candidates->story : '' }}</p>
    </div>   
</div>



    <div class="form-group">
      <label for="AmountNeeded">Enter Amount Needed:</label>
        <input type="number" class="form-control" name="AmountNeeded" id="AmountNeeded" value="{{ isset($candidates->amount) == true ? $candidates->amount : '0' }}" />
                    <span class="help-block">
            <strong id="AmountNeeded"></strong>
          </span>
    </div>


 @endif