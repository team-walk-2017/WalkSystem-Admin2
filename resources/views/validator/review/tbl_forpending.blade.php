@extends('layouts.main')
@section('content')
<link href="{{URL::asset('css/custom-table.css')}}" rel="stylesheet">
<div class="col-lg-12">
    @include('validator.review.contentForPending')
</div>



<!-- modal for the candidates -->
 <div id="candidates" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="candidatesView">
                {{ csrf_field() }}
                @include('validator.review.modal.view_candidates')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>






<!-- view modal para sa mga candidates -->
<script type="text/javascript">

	 function seecandidates(id){
        var url = "/validate/pending/candidates/" + id;
            $.ajax({
               url: url,
               type: 'get',            
               beforeSend: function() {
                $('#candidatesView').html('<center><img src="{!! URL::asset("/assets/images/loading.gif") !!}" height="100" width="100"></center>');
               },
               success: function(response) {
                $('#candidatesView').html(response);
               }
            });
            return false;
        }


</script>












@stop