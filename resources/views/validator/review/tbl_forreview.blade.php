@extends('layouts.main')
@section('content')
<link href="{{URL::asset('css/custom-table.css')}}" rel="stylesheet">
<div class="col-lg-12">
    @include('validator.review.contentForReview')
</div>



<!-- modal for the candidates -->
 <div id="candidates" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="candidatesView">
                {{ csrf_field() }}
                @include('validator.review.modal.view_candidates')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="rejectContent()">Reject</button>
                <button class="btn btn-success" type="submit" onclick="approveContent()">Approve</button>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop



@section('js')


<!-- view modal para sa mga candidates -->
<script type="text/javascript">

	 function seecandidates(id){
        var url = "/view-for-review-candidates/" + id;
            $.ajax({
               url: url,
               type: 'get',            
               beforeSend: function() {
                $('#candidatesView').html('<center><img src="{!! URL::asset("/assets/images/loading.gif") !!}" height="100" width="100"></center>');
               },
               success: function(response) {
                $('#candidatesView').html(response);
               }
            });
            return false;
        }


function rejectContent(){
   var id = $('.img-id').val();
   var token = $("[name='_token']").eq(0).val();
   var url = '/decline-candidates';
   $.ajax({
       url: url,
       type: 'post',
       data: {
           id: id,
           _token: token
       },
       beforeSend: function(){
           $('#candidatesView').html('<center><img src="{!! URL::asset("/assets/images/loading.gif") !!}" height="100" width="100"></center>');
       },
       success: function(response){
           // console.log(response);
           if(response){
               location.reload();
           }
       }
   });
}

function approveContent(){
   var id = $('.img-id').val();
   var token = $("[name='_token']").eq(0).val();
   var url = '/approve-candidates';
   var AmountNeeded = $('#AmountNeeded').val();
   $.ajax({
       url: url,
       type: 'post',
       data: {
           id: id,
           AmountNeeded: AmountNeeded,
           _token: token
       },
       beforeSend: function(){
           $('#candidatesView').html('<center><img src="{!! URL::asset("assets/images/loading.gif") !!}" height="100" width="100"></center>');
       },
       success: function(response){
           // console.log(response);
           if(response){
            
               $("#candidates").modal('hide');
               $.toast({
                 heading: 'Successfully validated.',
                 text: 'Candidate has been validated.',
                 position: 'top-right',
                 loaderBg:'#ff6849',
                 icon: 'success',
                 hideAfter: 3000, 
                 stack: 6
                });
                window.setTimeout(function(){location.reload()},3000);
           }
       }
   });
}

</script>


<!-- End message modal -->


@stop




