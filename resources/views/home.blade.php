 @extends('layouts.main')
 @section('content')
  @if(Auth::user()->rolename == "admin")

<script src="{{URL::asset('js/canvasjs.min.js')}}"></script>
<script type="text/javascript">

window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer", {
        title:{
            text: "Donation Received per Month"              
        },
        data: [              
        {
            // Change type to "doughnut", "line", "splineArea", etc.
            type: "column",
            dataPoints: {!!$data!!}
        }
        ]
    });
    chart.render();
}
</script>
</head>
<body>
<div id="chartContainer" style="height: 300px; width: 100%;"></div>

            {{-- end --}}

@else
    
   

  @endif


  
 @stop