<!-- Sidebar scroll-->
   @if(Auth::user()->rolename == "validator")
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="{{URL::asset('/assets/images/users/1.jpg')}}" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text">Validator <span class="caret"></span></a>
                    </div>
                </div>
                 <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PROFILE</li>
                        <li>
                            <a class="has-arrow" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();""><i class="fa fa-power-off"></i><span class="hide-menu">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                        </li>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li>
                            <a class="has-arrow" href="{{ route('review') }}" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">Candidates</a>
                        </li>
                        <li>
                            <a class="has-arrow " href="{{ route('pendingStories') }}" aria-expanded="false"><i class="icon-layers"></i><span class="hide-menu">Pending</span></a>
                        </li>

              

               @else {{-- admin landing page --}}
            <div class="scroll-sidebar">
                <div class="user-profile">
                        <!-- User profile image -->
                        <div class="profile-img"> <img src="{{URL::asset('/assets/images/users/1.jpg')}}" alt="user" /> </div>
                        <!-- User profile text-->
                        <div class="profile-text">Admin <span class="caret"></span></a>
                        </div>
                    </div>

                     <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PROFILE</li>
                        <li>
                            <a class="has-arrow" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();""><i class="fa fa-power-off"></i><span class="hide-menu">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                        </li>

              <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li>
                            <a class="has-arrow" href="{{ route('AcceptPending') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Pending Requests</a>
                        </li>
                         <li>
                            <a class="has-arrow" href="{{ route('approvedrequest') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Approved Requests</a>
                        </li>

                         <li>
                            <a class="has-arrow " href="{{ route('UploadSection') }}" aria-expanded="false"><i class="icon-layers"></i><span class="hide-menu">Upload</span></a>
                        </li>

                        <li>
                            <a class="has-arrow" href="{{url('/audit-trail')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Audit Trail</a>
                        </li>
                        

    @endif
            