@extends('layouts.main')
@section('content')
<center>
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>


    <div class="col-lg-6 col-md-6">
        <div class="card">
            <form class="form-horizontal form-material" method="POST" action="{{route('Upload')}}" enctype="multipart/form-data" name="the_form" id="EditProfile">
                {{csrf_field()}}
                <div class="card-body">
                    <h2 class="card-title">Upload Photo/Video</h2>
                    {{-- <label for="input-file-now">Your so fresh input file — Default version</label> --}}
                    <br>
                    <!-- <input type="file" id="input-file-now" class="dropify" /> -->
                    <!--  <input class="dropify" type="file" name="file" id="fileImg" multiple="multiple" accept="image" action="{{ url('/upload') }}" required> -->
                    <input type='file' onchange="readURL(this);" name="file" multiple="multiple"/>
                     <img id="ViewFile" src="#" alt="your image" />

                      @if($errors->has('file'))
                        <strong class="help-block">{{$errors->first('file')}}</strong>
                      @endif 
                </div>

                   <div class="form-group">
                    <textarea type="text" name="description" placeholder="Write a description" id="description" style="width: 350; height: 50;"></textarea>
                        <span class="help-block">
                            <strong id="description"></strong>
                        </span>
                    </div>
                <div class="col-lg-4 col-md-4">
                    <button type="submit" class="btn btn-block btn-success">Upload</button>
                 </div>
            </form>
         </div>
    </div>
</center>

@stop


<script type="text/javascript">
         function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ViewFile')
                        .attr('src', e.target.result)
                        .width(350)
                        .height(350);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>