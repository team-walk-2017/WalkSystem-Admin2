 @extends('layouts.main')
 @section('content')
<!-- <link href="{{URL::asset('css/custom-table.css')}}" rel="stylesheet"> -->


<style type="text/css">
 th {
   font-size: 15px;
}
td {
   font-size: 13px;
}
.action-width {
   width: 150px;
}

.description-width {
   width: 100px;
}

</style>

<div>
	

 	<div class="card">
        <div class="card-body">
             <div id="page-title">
         <h2 class="page-header text-overflow" style="">Audit Trail</h2>
     </div>
            <div class="table-responsive m-t-40">
                <table id="myTable" class="table table-bordered table-striped ">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>user name</th>
                            <th>event</th>
                            <th>old values</th>
                            <th>new values</th>
                            <th>url</th>
                            <th>ip address</th>
                            <th>date</th>
                            <th>time</th>
                        </tr>
                    </thead>
                    <tbody>
                    	 @foreach($logs as $log)
                        <tr>
                            <td>{{$log->id}}</td>
                            <td>{{$log->user_id}}</td>
                            <td>{{$log->event}}</td>
                            <td>{{json_encode(json_decode($log->old_values),JSON_PRETTY_PRINT)}}</td>
                  
                            <td><pre>{{json_encode(json_decode($log->new_values),JSON_PRETTY_PRINT)}}</pre></td>
                            <td>{{$log->url}}</td>
                            <td>{{$log->ip_address}}</td>
                            
                            <?php
                                $dateTime = explode(' ', $log->created_at);
                            ?>
                            <td>{{$dateTime[0]}}</td>
                            <td>{{$dateTime[1]}}</td>    
                        </tr>
                        @endforeach
					</tbody>
                </table>
            </div>
        </div>
    </div>
</div>


 	<script src="{{ URL::asset('/assets/plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script src="{{ URL::asset('/assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{ URL::asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ URL::asset('js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ URL::asset('js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ URL::asset('js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ URL::asset('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ URL::asset('js/custom.min.js') }}"></script>
    <!-- This is data table -->
    <script src="{{ URL::asset('/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
        });
    });
   
    </script>

 @stop