<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\UserResolver;

class stories extends Model implements Auditable, UserResolver
{

    use \OwenIt\Auditing\Auditable;

     protected $table ='stories';
      protected $fillable = [
        'id',
       'name',
       'amount',
       'email',
       'age',
       'gender',
       'address',
       'story',
       'mobile',
       'uploadedby',
       'status'
    ];
    public $incrementing = false;
    public $timestamps = false;

    public function approve($request){

       
       $acceptstories              = stories::findOrFail($request->id);
       $acceptstories->status      = 'P';
       $acceptstories->amount      = $request->AmountNeeded;
      

         if ($acceptstories->save()) {
          return redirect()->back();
         }else{
            return false;
         }

    }

    public function ActivatePendingStories($request){

        $acceptstories              = $this->find($request->id);
        $acceptstories->status      = 'A';


         if ($acceptstories->save()) {
            return redirect()->back();
         }else{
            return redirect()->back();
         }
    }

    public function deleteStories($request){

       $rejected = stories::findOrFail($request->id);
        $rejected->delete();


         if ($rejected->save()) {
            return redirect()->back();
         }else{
            return redirect()->back();
         }
    }

    public static function resolveId(){

      return \Auth::check() ? \Auth::user()->getAuthIdentifier() : null;

    }
    
}
