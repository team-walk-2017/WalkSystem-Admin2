<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
	 protected $table ='audits';
      protected $fillable = [
        'id',
       'user_id',
       'event',
       'auditable_id',
       'auditable_type',
       'old_values',
       'new_values',
       'url',
       'ip_address',
       'user_agent',
       'tags'
    ];
    public $incrementing = false;
    public $timestamps = false;
}
