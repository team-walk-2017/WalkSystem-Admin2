<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Http\Requests\Validation as validate;

class UploadModel extends Model implements Auditable, UserResolver
{
    use \OwenIt\Auditing\Auditable;

     protected $table ='upload_section';
      protected $fillable = [
       'beneficiary_id',
       'file_name',
       'description'
    ];
    public $incrementing = false;
    public $timestamps = false;

    public function UploadImageVideos(validate $validate, $request){
    	//upload image
        $file = $request->file;
        $destinationPath = public_path() .'/uploads';
        $originalFileName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension(); // getting file extension
        $filename = md5(rand(11111, 99999)) . md5(rand(11111, 99999)) . md5(rand(11111, 99999)) . '.' . $extension; // renameing image
      
        /*dd($filename);*/
        $upload_success=$file->move($destinationPath, $filename);  

        if ($upload_success) {

        	$upload = new UploadModel;
        	$upload->file_name = $filename;
            $upload->description = request('description');

        if ($upload->save()) {
            return redirect()->back();
        }else{
            return "false";
        }
    }
}

public static function resolveId(){

      return \Auth::check() ? \Auth::user()->getAuthIdentifier() : null;

    }

}