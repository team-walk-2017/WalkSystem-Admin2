<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stories;

class modalController extends Controller
{
     public function Viewcandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates_with_amount')->with('candidates',$candidates);
    }

     public function ValidatePendingCandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates')->with('candidates',$candidates);
    }


    public function ViewPendingCandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates_with_amount')->with('candidates',$candidates);
    }

    public function ViewApprovedCandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates')->with('candidates',$candidates);
    }

    public function deletestories(Request $request){
        
        $rejectStories = new stories();
        return $rejectStories->deleteStories($request);

       
    }

     public function approvestories(Request $request){
        
        /*$acceptstories = new stories();
        if($acceptstories->approve($request)){
            \Session::put('message','Pending candidate has been approved.');
           
            return redirect()->back();            
        }

        return \App::abort(500);*/

        $acceptstories = new stories();
        return $acceptstories->approve($request);


    }

    public function approvePendingStories(Request $request){
        

         $acceptPendingStories = new stories();
        return $acceptPendingStories->ActivatePendingStories($request);
    }
}
